#!/bin/sh
while true
do
	echo "Committing..."
	build_number=$(cat incrementer.txt)
	build_number=$((build_number+1))
	hash=$(openssl rand -base64 32)
	echo $hash >> hashed.txt
	echo $build_number > incrementer.txt
	git add .
	git commit -m "merge PR $build_number at #$hash"
	total_account=17
	for i in $(seq 1 $total_account); do
		echo "Push for remote no$i"
		git push "no$i" master
		w=$(awk -v min=60 -v max=90 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
		echo "Next push in $w seconds"
		sleep $w
	done
	echo "Build number #$build_number"
	duration=$(awk -v min=1400 -v max=1800 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
	echo "Next commit in $duration seconds..."
	sleep $duration
done
